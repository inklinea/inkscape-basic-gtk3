# Inkscape GTK3 Extension Template

▶ Inkscape GTK3 Extension Template

▶ Inkscape 1.1+

▶ Includes .py, .inx, .css, and .glade files for a basic GTK extension

![ALT](/Inkscape_GTK3_Extension_Template.png)
