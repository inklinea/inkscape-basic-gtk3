import inkex

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf


# -------------------------------------------------------------------------------
# HANDLERS
# -------------------------------------------------------------------------------

class Handler:
    def on_destroy(self, *args):
        Gtk.main_quit()

# -------------------------------------------------------------------------------
# END OF HANDLERS
# -------------------------------------------------------------------------------


# -------------------------------------------------------------------------------
# MAIN GTK3 LOOP
# -------------------------------------------------------------------------------

def run_gtk():
    # Load stylesheet
    screen = Gdk.Screen.get_default()
    provider = Gtk.CssProvider()
    provider.load_from_path("basic_gtk3.css")
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    global builder
    builder = Gtk.Builder()

    builder.add_from_file('basic_gtk3.glade')

    global window
    window = builder.get_object("main_window")
    builder.connect_signals(Handler())
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    Gtk.main()

# -------------------------------------------------------------------------------
# END OF MAIN GTK3 LOOP
# -------------------------------------------------------------------------------


class BasicGtk3(inkex.EffectExtension):

    def effect(self):
        run_gtk()


if __name__ == '__main__':
    BasicGtk3().run()
